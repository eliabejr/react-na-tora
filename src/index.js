import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import App from './pages/App'
import registerServiceWorker from './registerServiceWorker'

import './index.css'

ReactDOM.render(
  <BrowserRouter>
    <Switch>
      <Route path="/" exact={true} component={App} />
    </Switch>
  </BrowserRouter>,
  document.getElementById('root')
)
registerServiceWorker()