import React from 'react'
import styled from 'styled-components'
import { GoSync } from 'react-icons/go'

const SkillCardWrapper = styled.div `
    background-color: #fafafa;
    border-radius: 5px;
    color: #40739e;
    font-family: 'Lucida Sans Unicode', 'Arial', serif;
    max-height: 250px;
    max-width: 450px;
    min-height: 200px;
    overflow: hidden;
    padding: 10px;
    text-align: center;
    text-shadow: 0 0.05em 0.1em #555;
    width: 100%;
`

const FileMedia = styled(GoSync) `
    color: #fbc531;
    font-size: 6rem;
`

const Title = styled.h2 `
    font-size: 1.5rem;
`
const Text = styled.p `
    font-size: 0.9rem;
`

const SkillCard = () => (
    <SkillCardWrapper>
        <Title>Graphic Design</Title>
        <FileMedia />
        <Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sit amet dui eget lectus dignissim lacinia et sit amet lacus. Maecenas fringilla libero et risus ultricies, vel ornare leo egestas. Duis ut ipsum ut magna vestibulum dignissim iaculis bibendum arcu.</Text>
    </SkillCardWrapper>
)


export default SkillCard