import React, { Component } from 'react'
import SkillCard from '../components/SkillCard'


const App = () => (
  <div className="App">
    <header className="App-header">
      <h1 className="App-title">Welcome to React</h1>
    </header>
    <p className="App-intro">
      To get started, edit <code>src/App.js</code> and save to reload.
    </p>
    <SkillCard />
  </div>
)


export default App